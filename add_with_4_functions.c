#include<stdio.h>
int input1()
{
	int a;
printf("Enter a: ");
	scanf("%d", &a);
	return a;
}
int input2()
{
	int b;
	printf("Enter b: ");
	scanf("%d", &b);
	return b;
}
int Sum(int a, int b)
{
	int sum = a + b;
	return sum;
}
void output(int a, int b)
{
	printf("The sum of %d and %d is %d. \n", a, b, Sum(a, b));
}
int main()
{
	int a = input1();
	int b = input2();
	output(a, b);
}
