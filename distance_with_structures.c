#include<stdio.h>
#include<math.h>
struct Points
{
	int x, y;
};
typedef struct Points points;
points inputa(points a)
{
	scanf("%d%d", &a.x, &a.y);
	return a;
}
points inputb(points b)
{
	scanf("%d%d", &b.x, &b.y);
	return b;
}
float distance(points a, points b)
{
	float distance = sqrt(pow(b.y - a.y, 2) + pow(b.x - a.x, 2));
	return distance;
}
void output(points a, points b)
{
	printf("The distance between %d, %d and %d, %d is %f.\n", a.x, a.y, b.x, b.y, distance(a, b));
}
int main()
{
	points a, b;
	printf("Enter coordinates A: ");
	a = inputa(a);
	printf("Enter coordinates B: ");
	b = inputb(b);
	output(a, b);
	return 0;
}
