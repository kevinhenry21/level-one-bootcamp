#include<stdio.h>
float input()
{
	int a;
	scanf("%f",&a);
	return a;
}
float volume(float h, float d, float b)
{
	float vol = ((h*b*d) + (d/b))/3;
	return vol;
}
void output(float h, float b, float d)
{
	printf("The volume of tromboloid is %f\n",volume(h, b, d));
}
int main()
{
	float h, b, d;
	printf("Enter h: ");
	h = input();
	printf("Enter b: ");
	b = input();
	printf("Enter d: ");
	d = input();
	output(h, b, d);
	return 0;
}